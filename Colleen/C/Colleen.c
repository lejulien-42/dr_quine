#include <stdio.h>
#define FORMAT "#include <stdio.h>%1$c#define FORMAT %3$c%2$s%3$c%1$c%1$c/*%1$c	This string format the code%1$c*/%1$c%1$cvoid generate() {%1$c	printf(FORMAT, 10, FORMAT, 34);%1$c}%1$c%1$cint main(void) {%1$c	/*%1$c		Call generate function%1$c	*/%1$c	generate();%1$c}%1$c"

/*
	This string format the code
*/

void generate() {
	printf(FORMAT, 10, FORMAT, 34);
}

int main(void) {
	/*
		Call generate function
	*/
	generate();
}
