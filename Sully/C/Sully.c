#include <stdio.h>
#include <stdlib.h>
#include <strings.h>

#define FORMAT "#include <stdio.h>%1$c#include <stdlib.h>%1$c#include <strings.h>%1$c%1$c#define FORMAT %2$c%4$s%2$c%1$c%1$cint i=%5$d;%1$cchar nbuf[1024];%1$c%1$cint main() {%1$c	bzero(nbuf,1024);%1$c	sprintf(nbuf,%2$c./Sully_%3$cd.c%2$c,i-1);%1$c	FILE *o = fopen(nbuf,%2$cw%2$c);%1$c	fprintf(o,FORMAT,10,34,37,FORMAT,i-1);%1$c	fclose(o);%1$c	bzero(nbuf,1024);%1$c	if (i-1>0)%1$c		sprintf(nbuf,%2$cclang -Wall -Wextra -Werror ./Sully_%3$c1$d.c -o Sully_%3$c1$d;./Sully_%3$c1$d%2$c,i-1);%1$c	else%1$c		sprintf(nbuf,%2$cclang -Wall -Wextra -Werror ./Sully_%3$c1$d.c -o Sully_%3$c1$d%2$c,i-1);%1$c	system(nbuf);%1$c}%1$c"

int i=5;
char nbuf[1024];

int main() {
	bzero(nbuf,1024);
	sprintf(nbuf,"./Sully_%d.c",i-1);
	FILE *o = fopen(nbuf,"w");
	fprintf(o,FORMAT,10,34,37,FORMAT,i-1);
	fclose(o);
	bzero(nbuf,1024);
	if (i-1>0)
		sprintf(nbuf,"clang -Wall -Wextra -Werror ./Sully_%1$d.c -o Sully_%1$d;./Sully_%1$d",i-1);
	else
		sprintf(nbuf,"clang -Wall -Wextra -Werror ./Sully_%1$d.c -o Sully_%1$d",i-1);
	system(nbuf);
}
