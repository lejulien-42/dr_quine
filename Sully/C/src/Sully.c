#include <stdio.h>
#include <stdlib.h>
#include <strings.h>

char f[]="#include <stdio.h>%c#include <stdlib.h>%c#include <strings.h>%c%cchar f[]=%c%s%c;%cint i = %d;%cchar nbuf[1024];%c%cint main(int ac, char **av) {%c		// get values%c		i = (ac == 2)?atoi(av[1]):i;%c		if (i < 0) return 0;%c		// open file%c		bzero(nbuf, 1024);%c		sprintf(nbuf, %c./Sully_%cd.c%c, i);%c		FILE *o = fopen(nbuf, %cw%c);%c		// write code to other%c		fprintf(o, f, 10, 10, 10, 10, 34, f, 34, 10, i, 10, 10, 10, 10, 10, 10, 10, 10, 10, 34, 37, 34, 10, 34, 34, 10, 10, 10, 10, 10, 10, 10, 34, 37, 37, 37, 37, 34, 10, 10, 34, 37, 37, 37, 37, 34, 10, 10, 34, 37, 37, 34, 10, 10, 10, 10);%c		fclose(o);%c		// prepare compilation settings%c		bzero(nbuf, 1024);%c		if (ac == 1)%c			sprintf(nbuf, %cclang -Wall -Wextra -Werror ./Sully_%cd.c -o Sully_%cd;./Sully_%cd %cd%c, i, i, i, i);%c		else if (i > 0)%c			sprintf(nbuf, %cclang -Wall -Wextra -Werror ./Sully_%cd.c -o Sully_%cd;./Sully_%cd %cd%c, i, i, i, i-1);%c		else%c			sprintf(nbuf, %cclang -Wall -Wextra -Werror ./Sully_%cd.c -o Sully_%cd%c, i, i);%c		// execute%c		system(nbuf);%c}%c";
int i = 5;
char nbuf[1024];

int main(int ac, char **av) {
		// get values
		i = (ac == 2)?atoi(av[1]):i;
		if (i < 0) return 0;
		// open file
		bzero(nbuf, 1024);
		sprintf(nbuf, "./Sully_%d.c", i);
		FILE *o = fopen(nbuf, "w");
		// write code to other
		fprintf(o, f, 10, 10, 10, 10, 34, f, 34, 10, i, 10, 10, 10, 10, 10, 10, 10, 10, 10, 34, 37, 34, 10, 34, 34, 10, 10, 10, 10, 10, 10, 10, 34, 37, 37, 37, 37, 34, 10, 10, 34, 37, 37, 37, 37, 34, 10, 10, 34, 37, 37, 34, 10, 10, 10, 10);
		fclose(o);
		// prepare compilation settings
		bzero(nbuf, 1024);
		if (ac == 1)
			sprintf(nbuf, "clang -Wall -Wextra -Werror ./Sully_%d.c -o Sully_%d;./Sully_%d %d", i, i, i, i);
		else if (i > 0)
			sprintf(nbuf, "clang -Wall -Wextra -Werror ./Sully_%d.c -o Sully_%d;./Sully_%d %d", i, i, i, i-1);
		else
			sprintf(nbuf, "clang -Wall -Wextra -Werror ./Sully_%d.c -o Sully_%d", i, i);
		// execute
		system(nbuf);
}
